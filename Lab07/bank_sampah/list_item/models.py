from django.db import models
from django.conf import settings
from django.utils import timezone
import datetime
import locale

# Create your models here.
class setoran_sampah(models.Model):
    tanggal = models.DateTimeField(default=datetime.datetime.now)
    #waktu = models.TimeField()
    penyetor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    jenis_sampah = (
        ('Plastik','Plastik'),
        ('Ember','Ember'),
        ('Kardus','Kardus'),
        ('Botol','Botol'),
    )
    jenis = models.CharField(max_length=10, choices=jenis_sampah)
    jumlah_setoran = models.IntegerField()
    total_poin = models.IntegerField()
    pilihan_status = (
        ('Approved','Approved'),
        ('Waiting','Waiting'),
    )
    status = models.CharField(max_length=10, choices=pilihan_status)   
    saldo = models.CharField (max_length=20, editable=False, blank=True, null=True)
    def save(self, *args, **kwargs):
        self.saldo = self.total_poin * 100
        self.saldo = f"{self.saldo:,}"
        self.saldo = str("Rp " + self.saldo)
        super(setoran_sampah, self).save(*args, **kwargs)
    
    
    
    # Ini fungsinya buat apa yaa??
    class Meta:
        verbose_name_plural = "setoran_sampah"
        ordering = ['tanggal']
    
    def __str__(self):
        return self.penyetor.username
    
# class konversi_uang(models.Model):
#     saldo = models.ForeignKey(setoran_sampah, on_delete=models.PROTECT)
