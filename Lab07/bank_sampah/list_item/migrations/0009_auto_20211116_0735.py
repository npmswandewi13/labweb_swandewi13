# Generated by Django 3.2.8 on 2021-11-16 00:35

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('list_item', '0008_alter_setoran_sampah_tanggal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='setoran_sampah',
            name='tanggal',
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AlterField(
            model_name='setoran_sampah',
            name='waktu',
            field=models.TimeField(default=datetime.timezone),
        ),
    ]
