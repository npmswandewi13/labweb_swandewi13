import React, { useState, useEffect } from "react";
import axios from "axios";
import Nav from "../components/nav";

const CreateResep = () => {
    const [Judul, setJudul] = useState(null)
    const [Deskripsi, setDeskripsi] = useState(null)
    const [Tipe, setTipe] = useState(null)
    const [WaktuPembuatan, setWaktuPembuatan] = useState(null)
    const [TingkatKesusahan, setTingkatKesusahan] = useState("mudah")

    const [ListTipe, setListTipe] = useState([])

    const value = {
        tipe: Tipe,
        judul: Judul,
        deskripsi: Deskripsi,
        waktu_pembuatan: WaktuPembuatan,
        tingkat_kesusahan: TingkatKesusahan,
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        axios.post('http://127.0.0.1:8000/api/resepMakanan/', value)
        .then(response => {
            console.log(response)
        })
    }

    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api/tipeMakanan/')
        .then(response =>{
            setTipe(response.data[0].id)
            setListTipe(response.data)
        })
    }, [])

    return (
        <div>
            <Nav/>
            <form onSubmit={handleSubmit}>
            <div className="resep-create">
                <h2>Create Resep</h2>
                <h4>Judul</h4> 
                <input
                    type="text"
                    value={Judul}
                    onChange={(event) => setJudul(event.target.value)}
                />
                
                <h4>Deskripsi</h4>
                <input 
                    type="text"
                    value={Deskripsi}
                    onChange={(event) => setDeskripsi(event.target.value)}
                />
                
                <h4>Tipe</h4>
                <select 
                    value={Tipe}
                    onChange={(event) => setTipe(event.target.value)}>
                    {ListTipe.map((item) => {
                        return (
                            <option key={item.id} value={item.id}>{item.tipe} {item.judul}</option>
                        )
                    })}
                </select>
                
                <h4>Waktu Pembuatan</h4>
                <input
                    type="text"
                    value={WaktuPembuatan}
                    onChange={(event) => setWaktuPembuatan(event.target.value)}
                />
                
                <h4>Tingkat Kesusahan</h4>
                <select value={TingkatKesusahan} onChange={(event) => setTingkatKesusahan(event.target.value)}>
                    <option value="mudah">mudah</option>
                    <option value="sedang">sedang</option>
                    <option value="susah">susah</option>
                </select>
                
                <button type="Submit" style={{display: 'block'}}>Submit</button>  
            </div>    
            </form>
        </div>
    )
}

export default CreateResep;