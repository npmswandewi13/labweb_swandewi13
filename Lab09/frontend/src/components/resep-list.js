import React, {useState, useEffect} from "react";
import axios from "axios";
import './styles.css'

const ResepList = () => {
    const [resepMakanan, setResepMakanan] = useState([])

    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api/resepMakanan/')
            .then(response => {
            setResepMakanan(response.data)
        })
    }, [])

    return (
        <div className="resep-box">
            <h2 class="title">Berbagai Resep Makanan</h2>
            {resepMakanan.map((item) => {
                return (
                    <div className="resep-content" key={item.id}>
                        <h3>{item.judul}</h3>
                        <p>{item.deskripsi}</p>
                        <p>waktu pembuatan : {item.waktu_pembuatan} menit</p>
                        <p>tipe makanan : {item.tipe}</p>
                        <p>tingkat kesusahan : {item.tingkat_kesusahan}</p>
                    </div>
                )
            })}       
        </div>
    )
}

export default ResepList;