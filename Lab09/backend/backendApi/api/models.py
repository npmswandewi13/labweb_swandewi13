from django.db import models

# Create your models here.
class tipeMakanan(models.Model):
    judul = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=400)
    
    def __str__(self):
        return str(self.judul)
    
class resepMakanan(models.Model):
    judul = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=400)
    tipe = models.ForeignKey(tipeMakanan, on_delete=models.CASCADE)
    waktu_pembuatan = models.IntegerField()
    tingkat = (
        ('mudah','mudah'),
        ('sedang','sedang'),
        ('susah','susah'),
    )
    
    tingkat_kesusahan = models.CharField(max_length=10, choices=tingkat)
    
    def __str__(self):
        return str(self.judul)
    
class bahanMakanan(models.Model):
    resep = models.ForeignKey(resepMakanan, on_delete=models.CASCADE)
    judul = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=200)
    jumlah = models.IntegerField()
    satuan = models.CharField(max_length=10)
    
    def __str__(self):
        return str(self.judul)
    