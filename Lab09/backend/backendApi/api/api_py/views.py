from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import ApiModelSerializer, ApiModelSerializer2
from .. import models

# Create your views here.

@api_view(['GET', 'POST'])
def api_list(request):
    if request.method == 'GET':
        qs = models.resepMakanan.objects.all()
        serializer = ApiModelSerializer(qs, many=True)
        return Response(serializer.data)
    
    elif request.method == 'POST':
        serializer = ApiModelSerializer(data=request.data)
        if serializer.is_valid():
            tipe = models.tipeMakanan.objects.get(id=request.data['tipe'])
            serializer.save(tipe=tipe)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def api_list2(request):
    if request.method == 'GET':
        qs = models.tipeMakanan.objects.all()
        serializer = ApiModelSerializer2(qs, many=True)
        return Response(serializer.data)
    
    elif request.method == 'POST':
        serializer = ApiModelSerializer2(data=request.data)
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


