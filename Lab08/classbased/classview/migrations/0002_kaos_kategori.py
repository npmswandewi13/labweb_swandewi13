# Generated by Django 3.2 on 2021-11-22 04:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classview', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='kaos',
            name='kategori',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='classview.kategori_kaos'),
        ),
    ]
