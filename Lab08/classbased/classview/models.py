from django.db import models
from django.urls import reverse

# Create your models here.
class kategori_kaos(models.Model):
    nama = models.CharField(max_length=200)
    
    def __str__(self):
        return self.nama
    
class bahan_kaos(models.Model):
    nama = models.CharField(max_length=30)
    
    def __str__(self):
        return self.nama

class kaos(models.Model):
    nama =  models.CharField(max_length=100)
    ukuran_kaos = [
        ('S','S'),
        ('M','M'),
        ('L','L'),
        ('XL','XL'),
        ('XLL','XLL'),
    ]
    ukuran = models.CharField(max_length=10, choices=ukuran_kaos)
    bahan = models.ForeignKey(bahan_kaos, on_delete=models.CASCADE, null=True)
    kategori = models.ForeignKey(kategori_kaos, on_delete=models.CASCADE, null=True)
    harga = models.IntegerField(default=0)
    stok = models.IntegerField(default=0)
    
    def __str__(self):
        return self.nama
    
    def get_absolute_url(self):
        return reverse("home")
    
    