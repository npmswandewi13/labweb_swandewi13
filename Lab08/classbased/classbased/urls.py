"""classbased URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from classview import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.homeView.as_view(), name="home"),
    path('kaos/<int:pk>',views.kaosView.as_view(), name="kaos"),
    path('kaos/update/<int:pk>',views.updateKaos.as_view(), name="updatekaos"),
    path('kaos/delete/<int:pk>',views.deleteKaos.as_view(), name="deletekaos"),
    path('kaos/add',views.addKaos.as_view(), name="addkaos"),
]
