from django.db import models

# Create your models here.
class kopi(models.Model):
    nama = models.CharField(max_length=50)
    gambar = models.ImageField(upload_to="images")
    stok = models.IntegerField(default=0)
    tersedia = models.BooleanField(default=True)
    variasi_merek = [
        ('Nescafe', 'Nescafe'),
        ('Kapal Api', 'Kapal Api'),
        ('Indocafe', 'Indocafe'),
        ('Torabika', 'Torabika'),
    ]

    merek = models.CharField(
        max_length=10,
        choices=variasi_merek,
        default='NES'
    )

    def __str__(self):
        return self.nama