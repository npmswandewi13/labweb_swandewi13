from django.shortcuts import render
from . import models

# Create your views here.
def home(request):
    daftar_kopi = models.kopi.objects.all()

    context = {
        'daftar_kopi': daftar_kopi,
    }

    return render(request, 'home.html', context)